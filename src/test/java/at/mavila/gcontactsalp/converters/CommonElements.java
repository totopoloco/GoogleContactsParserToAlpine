package at.mavila.gcontactsalp.converters;

import at.mavila.gcontactsalp.pojos.AddressBook;
import org.apache.log4j.BasicConfigurator;

import java.util.ArrayList;
import java.util.List;

public class CommonElements {

    protected static final List<AddressBook> ADDRESS_BOOK_LIST;

    static {
        BasicConfigurator.configure();

        ADDRESS_BOOK_LIST = new ArrayList<>();

        List<String> email1 = new ArrayList<>();
        email1.add("hola0@hola.com");
        email1.add("hola1@hola.com");
        List<String> email2 = new ArrayList<>();
        email2.add("hola2@hola.com");
        email2.add("hola3@hola.com");
        ADDRESS_BOOK_LIST.add(new AddressBook("ana", "Bolena Manzanillo", email1, "comment0"));
        ADDRESS_BOOK_LIST.add(new AddressBook("beatriz", "Ramirez Lopez", email2, "comment1"));

    }

}
