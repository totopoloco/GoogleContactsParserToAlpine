package at.mavila.gcontactsalp.converters;

import at.mavila.gcontactsalp.pojos.AddressBook;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.function.Function;

/**
 * Created by mavila on 1/30/17.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class AlpineConverterTest extends CommonElements{

    private static final Logger LOGGER = LoggerFactory.getLogger(AlpineConverterTest.class);

    @Autowired
    private Function<List<AddressBook>, CharSequence> alpineConverter;


    @Test
    public void convertAndCompareName() {

         final CharSequence convert = this.alpineConverter.apply(ADDRESS_BOOK_LIST);
        LOGGER.info("Array: {}: ", ADDRESS_BOOK_LIST);
        LOGGER.info("Result: {}: ", convert);
        Assert.assertThat("Convert has some of the string", convert.toString(), Matchers.containsString("ana"));

    }


    @Test
    public void convertAndCompareAnotherName() {

         final CharSequence convert = this.alpineConverter.apply(ADDRESS_BOOK_LIST);
        LOGGER.info("Array: {}: ", ADDRESS_BOOK_LIST);
        LOGGER.info("Result: {}: ", convert);

        Assert.assertThat("Convert has some of the string", convert.toString(), Matchers.containsString("beatriz"));

    }


}