package at.mavila.gcontactsalp.converters;

import at.mavila.gcontactsalp.pojos.AddressBook;
import org.cactoos.io.ResourceOf;
import org.cactoos.text.TextOf;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.function.Function;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
public class XmlToListAddressBookConverterTest extends CommonElements {

    private static final Logger LOGGER = LoggerFactory.getLogger(XmlToListAddressBookConverterTest.class);

    @Autowired
    private Function<CharSequence, List<AddressBook>> xmlToListAddressBookConverter;


    @Test
    public void whenNullParameterThenEmptyList() {
        final List<AddressBook> addressBookList = xmlToListAddressBookConverter.apply(null);
        if(LOGGER.isInfoEnabled()) LOGGER.info(addressBookList.toString());
        assertThat(addressBookList).isEmpty();
    }

    @Test
    public void whenInvalidParameterThenEmptyList() {
        final List<AddressBook> addressBookList = xmlToListAddressBookConverter.apply("hello");
        if(LOGGER.isInfoEnabled()) LOGGER.info(addressBookList.toString());
        assertThat(addressBookList).isEmpty();
    }

    @Test
    public void whenValidParameterThenNotEmptyList() {
        try {
            final List<AddressBook> addressBookList = xmlToListAddressBookConverter.apply(
                    new TextOf(new ResourceOf("mutt.xml"), StandardCharsets.UTF_8).asString()
            );
            if(LOGGER.isInfoEnabled()) LOGGER.info(addressBookList.toString());
            assertThat(addressBookList).isNotEmpty();
            assertThat(addressBookList).hasSize(3);
        } catch (Exception e) {
            LOGGER.error(String.format("Exception caught: <%s>", e.getMessage()), e);
            assertThat(true).isFalse();
        }
    }
}