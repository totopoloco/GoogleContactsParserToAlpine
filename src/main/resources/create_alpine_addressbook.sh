#!/bin/bash

ADDRESSBOOK=$HOME/.addressbook
DIST_FOLDER=$HOME/dist
DUMPED_FILE=mutt.xml
INVALID_JAVA_VERSION="1.7"

if [ -e "$ADDRESSBOOK" ] && [ ! -z "$ADDRESSBOOK" ]
then
    echo "$ADDRESSBOOK exists and is not empty, will be deleted to create a new one"
    rm -f $ADDRESSBOOK 
fi


echo "Starting to create a new address book for Alpine in: $ADDRESSBOOK"

JAVA_VERSION=$(java -version 2>&1 |awk 'NR==1{ gsub(/"/,""); print $3 }')
#JAVA_VERSION="1.7"

if [ ! -z "$JAVA_VERSION" ]
then
	if [[ "$JAVA_VERSION" > "1.7" ]]; then
		echo "Java version in this system: $JAVA_VERSION [OK]"
		cd $DIST_FOLDER
		goobook dump_contacts > $DUMPED_FILE
		
		if [ -e "$DUMPED_FILE" ] && [ ! -z "$DUMPED_FILE" ]
		then
			echo "$DUMPED_FILE is not empty, calling curl now [OK]"
                        curl google-parser-alpine.cfapps.io/importAlpine -d @$DUMPED_FILE --header "Content-Type:text/xml" -o addressbook.txt
			#java -jar GoogleContactsParserToAlpine-2.0.jar > /dev/null 2>&1
			rm -f $DUMPED_FILE
                        echo "Moving generated address book"
                        mv addressbook.txt $ADDRESSBOOK
                        echo "Done."
		fi
	else         
        	echo "Java version must be bigger than $INVALID_JAVA_VERSION to work properly"
	fi
else
	echo "Java was NOT found on this system, please install Java before proceeding"
fi

