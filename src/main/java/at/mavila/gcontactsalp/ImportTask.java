package at.mavila.gcontactsalp;

import at.mavila.gcontactsalp.pojos.AddressBook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.InputStream;
import java.util.Collections;
import java.util.List;
import java.util.function.Function;

/**
 * Created by 200000313 on 23.12.2016.
 */
@Component("importTask")
public class ImportTask {

    private final Function<InputStream, List<AddressBook>> parser;

    @Autowired
    public ImportTask(final Function<InputStream, List<AddressBook>> parser) {
        this.parser = parser;
    }

    /**
     * 1. Parses the input stream (XML file)
     * 2. Gets the tokens
     * 3. Sorts them
     * 4. Writes the token to the file system.
     *
     * @param stream represents the xml input.
     * @return list of address book objects.
     */
    public List<AddressBook> doImport(final InputStream stream) {
        List<AddressBook> tokens = this.parser.apply(stream);
        Collections.sort(tokens);
        return tokens;
    }

}
