package at.mavila.gcontactsalp.aspect;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by 200000313 on 23.12.2016.
 */
@Aspect
@Component
public class ImportTaskAspect {

    private static final Logger LOGGER = LoggerFactory.getLogger(ImportTaskAspect.class);
    private static final ThreadLocal<SimpleDateFormat> FORMAT_THREAD_LOCAL =
            ThreadLocal.withInitial(() -> new SimpleDateFormat("yyyy-MM-dd'T'HH:mm'Z'"));

    @Before("execution(* at.mavila.gcontactsalp.ImportTask.doImport(..))")
    public void beforeImport(final JoinPoint joinPoint) {
        if(LOGGER.isInfoEnabled()) {
            LOGGER.info("Starting import at {}. Joint Point: {}", FORMAT_THREAD_LOCAL.get().format(new Date()), joinPoint.getSignature());
        }
    }


    @After("execution(* at.mavila.gcontactsalp.ImportTask.doImport(..))")
    public void afterImport(final JoinPoint joinPoint) {
        if(LOGGER.isInfoEnabled()) {
            LOGGER.info("Ending import at {}. Joint Point: {}", FORMAT_THREAD_LOCAL.get().format(new Date()), joinPoint.getSignature());
        }
    }

    @AfterThrowing(
            pointcut = "execution(* at.mavila.gcontactsalp.ImportTask.doImport(..))",
            throwing = "ex")
    public void doRecoveryActions(Exception ex) {

        LOGGER.error("Exception caught in doRecoveryActions: {}",ex.getMessage());

    }

}
