package at.mavila.gcontactsalp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;

@SpringBootApplication
@Import({ ImportTask.class })
@ComponentScan
public class GoogleContactsParserToAlpineApplication {

    public static void main(String[] args) {
        SpringApplication.run(GoogleContactsParserToAlpineApplication.class, args);
    }
}
