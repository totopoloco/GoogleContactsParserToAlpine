package at.mavila.gcontactsalp.exceptions;

public class ParserException extends Exception {

    public static final long serialVersionUID = 8288384L;

    public ParserException() {
        super();
    }

    public ParserException(String message) {
        super(message);
    }

    public ParserException(String message, Throwable cause) {
        super(message, cause);
    }

    public ParserException(Throwable cause) {
        super(cause);
    }
}
