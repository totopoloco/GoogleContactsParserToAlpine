package at.mavila.gcontactsalp.exceptions;

/**
 * Created by mavila on 22.12.16.
 */
class InvalidCommandLineArgumentException extends RuntimeException {

    /**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 7897825251450237950L;


	public InvalidCommandLineArgumentException(){
        super();
    }


    public InvalidCommandLineArgumentException(final String message){
        super(message);
    }

}