package at.mavila.gcontactsalp.converters.alpine;

import org.springframework.stereotype.Component;

import at.mavila.gcontactsalp.pojos.AddressBook;

/**
 * AlpineConverterNoAddresses.
 *
 * @author mavila
 */
@Component("alpineConverterNoAddresses")
public class AlpineConverterNoAddresses extends AlpineConverterCommon {

    @Override
    public StringBuilder apply(AddressBook addressBook) {
        final StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(this.startString(addressBook));
        stringBuilder.append(" ");
        stringBuilder.append(this.endString(addressBook));
        return stringBuilder;
    }
}
