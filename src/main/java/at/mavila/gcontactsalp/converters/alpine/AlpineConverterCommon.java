package at.mavila.gcontactsalp.converters.alpine;

import at.mavila.gcontactsalp.pojos.AddressBook;

import java.util.function.Function;

import static java.util.Optional.ofNullable;

/**
 * AlpineConverterCommon.
 *
 * @author mavila
 */
abstract class AlpineConverterCommon implements Function<AddressBook, StringBuilder> {

    protected StringBuilder startString(final AddressBook addressBook) {
        final AddressBook addressBookClean = ofNullable(addressBook)
                .orElseThrow(() -> new IllegalArgumentException("AddressBook must not be null"));
        final StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(addressBookClean.getNickname()).append("\t").append(" ");
        stringBuilder.append(addressBookClean.getFullName()).append("\t").append(" ");
        return stringBuilder;
    }

    protected StringBuilder endString(final AddressBook addressBook) {
        final StringBuilder stringBuilder = new StringBuilder();
        stringBuilder
                .append("\t\t")
                .append(" ")
                .append(ofNullable(addressBook).orElseThrow(() -> new IllegalArgumentException("AddressBook must not be null")).getComment())
                .append("\r\n");
        return stringBuilder;
    }
}
