package at.mavila.gcontactsalp.converters.alpine;

import at.mavila.gcontactsalp.pojos.AddressBook;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * AlpineConverterManyAddresses.
 *
 * @author mavila
 */
@Component("alpineConverterManyAddresses")
public class AlpineConverterManyAddresses extends AlpineConverterNoAddresses {

    private static final String DELIMITER = ", ";
    private static final Function<List<String>, String> ADDRESSES_TO_STRING_CONVERTER = addresses -> addresses.stream().collect(Collectors.joining(DELIMITER));

    @Override
    public StringBuilder apply(AddressBook addressBook) {

        final StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(this.startString(addressBook));
        final List<String> addresses = addressBook.getAddress();

        Collections.sort(addresses);
        stringBuilder.
                append("(").
                append(ADDRESSES_TO_STRING_CONVERTER.apply(addresses)).
                append(")");

        stringBuilder.append(this.endString(addressBook));

        return stringBuilder;
    }


}
