package at.mavila.gcontactsalp.converters.alpine;

import org.springframework.stereotype.Component;

import at.mavila.gcontactsalp.pojos.AddressBook;

/**
 * AlpineConverterOneAddress.
 *
 * @author mavila
 */
@Component("alpineConverterOneAddress")
public class AlpineConverterOneAddress extends AlpineConverterNoAddresses {

    @Override
    public StringBuilder apply(AddressBook addressBook) {
        final StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(this.startString(addressBook));
        stringBuilder.append(addressBook.getAddress().get(0));
        stringBuilder.append(this.endString(addressBook));
        return stringBuilder;
    }
}
