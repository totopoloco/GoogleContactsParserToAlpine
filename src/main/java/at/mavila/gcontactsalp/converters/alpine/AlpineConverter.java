package at.mavila.gcontactsalp.converters.alpine;

import at.mavila.gcontactsalp.pojos.AddressBook;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Objects;
import java.util.function.Function;

/**
 * AlpineConverter.
 * <p>
 * Created by mavila on 1/28/17.
 */
@Component("alpineConverter")
public class AlpineConverter implements Function<List<AddressBook>, CharSequence> {

    private static final Logger LOGGER = LoggerFactory.getLogger(AlpineConverter.class);

    private final AlpineConverterNoAddresses alpineConverterNoAddresses;
    private final AlpineConverterOneAddress alpineConverterOneAddress;
    private final AlpineConverterManyAddresses alpineConverterManyAddresses;

    @Autowired
    public AlpineConverter(
            final AlpineConverterNoAddresses alpineConverterNoAddresses,
            final AlpineConverterOneAddress alpineConverterOneAddress,
            final AlpineConverterManyAddresses alpineConverterManyAddresses) {

        this.alpineConverterNoAddresses = alpineConverterNoAddresses;
        this.alpineConverterOneAddress = alpineConverterOneAddress;
        this.alpineConverterManyAddresses = alpineConverterManyAddresses;

    }


    @Override
    public CharSequence apply(final List<AddressBook> addressBooks) {

        if (CollectionUtils.isEmpty(addressBooks)) {
            return StringUtils.EMPTY;
        }

        final StringBuilder stringBuilder = new StringBuilder();
        addressBooks.stream()
                    .filter(Objects::nonNull)
                    .forEach(addressBook -> stringBuilder.append(createRecordForAddressBook(addressBook)));

        if(LOGGER.isDebugEnabled()) LOGGER.debug("Finished converting, returning {}", stringBuilder);
        return stringBuilder;
    }

    /**
     * Creates a record.
     *
     * @param addressBook the address book already parsed, information to be extracted.
     */
    private  CharSequence createRecordForAddressBook(final AddressBook addressBook) {

        if (CollectionUtils.isEmpty(addressBook.getAddress())) {
            return this.alpineConverterNoAddresses.apply(addressBook);
        }

        if (addressBook.getAddress().size() == 1) {
            return this.alpineConverterOneAddress.apply(addressBook);
        }

        return this.alpineConverterManyAddresses.apply(addressBook);
    }

}



