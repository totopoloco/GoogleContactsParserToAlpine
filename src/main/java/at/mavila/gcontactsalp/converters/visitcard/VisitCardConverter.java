package at.mavila.gcontactsalp.converters.visitcard;

import at.mavila.gcontactsalp.pojos.AddressBook;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * Converts the List of Address book to a CharSequence.
 * <p>
 * Created by mavila on 4/16/17.
 */
@Component("visitCardConverter")
public class VisitCardConverter implements Function<List<AddressBook>, CharSequence> {


    private static final Pattern NAME_SPLIT = Pattern.compile("(.+)(\\s+)(.+)");
    private static final int INITIAL_VALUE_1 = 1;
    private static final Logger LOGGER = LoggerFactory.getLogger(VisitCardConverter.class);

    private CharSequence convert(final List<AddressBook> addressBooks) {

        if (CollectionUtils.isEmpty(addressBooks)) {
            LOGGER.warn("Address book is empty, returning an empty string.");
            return StringUtils.EMPTY;
        }

        final StringBuilder stringBuilder = new StringBuilder();


        addressBooks
                .stream()
                .filter(Objects::nonNull)
                .forEach(addressBook -> {

                    stringBuilder.append("begin:vcard\r\n");
                    stringBuilder.append("fn:").append(addressBook.getFullName()).append("\r\n");
                    stringBuilder.append("n:").append(createNamePart(addressBook.getFullName())).append("\r\n");

                    final AtomicInteger atomicInteger = new AtomicInteger(INITIAL_VALUE_1);
                    addressBook
                            .getAddress()
                            .stream()
                            .filter(Objects::nonNull)
                            .forEach(emailAddress ->
                                    stringBuilder
                                            .append("email;internet;")
                                            .append(atomicInteger.getAndIncrement() == INITIAL_VALUE_1 ? "HOME:" : "WORK:")
                                            .append(emailAddress).append("\r\n"));

                    if (isNotAddressBookCommentContainsGoogle(addressBook)) {
                        stringBuilder.append("note:").append(addressBook.getComment()).append("\r\n");
                    }

                    stringBuilder.append("version:2.1\r\n");
                    stringBuilder.append("end:vcard\r\n");
                    stringBuilder.append("\r\n");
                });


        return stringBuilder.toString();
    }

    private boolean isNotAddressBookCommentContainsGoogle(final AddressBook addressBook) {
        return !addressBook.getComment().contains("Google");
    }


    private String createNamePart(final String fullName) {

        if (StringUtils.isEmpty(fullName)) {
            return StringUtils.EMPTY;
        }


        final Matcher matcher = NAME_SPLIT.matcher(fullName);

        if (matcher.find()) {
            return matcher.group(3) + ";" + matcher.group(1);
        }


        return fullName + ";";


    }

    @Override
    public CharSequence apply(List<AddressBook> addressBooks) {
        return this.convert(addressBooks);
    }
}