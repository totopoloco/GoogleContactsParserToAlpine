package at.mavila.gcontactsalp.converters.input;

import at.mavila.gcontactsalp.ImportTask;
import at.mavila.gcontactsalp.pojos.AddressBook;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.ByteArrayInputStream;
import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Consumer;
import java.util.function.Function;

import static org.apache.commons.collections4.CollectionUtils.isEmpty;

@Component("xmlToListAddressBookConverter")
public class XmlToListAddressBookConverter implements Function<CharSequence, List<AddressBook>> {

    private static final Logger LOGGER = LoggerFactory.getLogger(XmlToListAddressBookConverter.class);
    private final ImportTask importTask;

    @Autowired
    public XmlToListAddressBookConverter(
            final ImportTask importTask
    ) {
        this.importTask = importTask;
    }


    @Override
    public List<AddressBook> apply(CharSequence xml) {
        AtomicReference<List<AddressBook>> listAtomicReference = new AtomicReference<>(Collections.emptyList());

        Optional.ofNullable(xml)
                .filter(StringUtils::isNotBlank)
                .ifPresent(getCharSequenceConsumer(listAtomicReference));


        final List<AddressBook> addressBooks = listAtomicReference.get();

        if (isEmpty(addressBooks)) {
            LOGGER.warn("Returning an empty list");
        }

        return addressBooks;

    }

    private Consumer<CharSequence> getCharSequenceConsumer(AtomicReference<List<AddressBook>> listAtomicReference) {
        return charSequence -> {
            final List<AddressBook> addressBooks = this.importTask
                    .doImport(new ByteArrayInputStream(charSequence.toString()
                            .getBytes(StandardCharsets.UTF_8)));

            listAtomicReference.set(addressBooks);
        };
    }
}