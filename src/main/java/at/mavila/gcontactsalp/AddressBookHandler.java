package at.mavila.gcontactsalp;

import at.mavila.gcontactsalp.pojos.AddressBook;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static at.mavila.gcontactsalp.AddressBookHandlerConstants.*;

/**
 * Created by mavila on 22.12.16.
 */
class AddressBookHandler extends DefaultHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(AddressBookHandler.class);
    private static final char SPACE_CHAR = ' ';
    private static final char UNDERSCORE_CHAR = '_';
    private boolean isEntry = false;
    private boolean isFullName = false;
    private boolean isEmail = false;
    private boolean isPhoneNumber = false;
    private String fullName = null;
    private String phoneNumber = null;

    //Lists
    private List<String> emails = new ArrayList<>();
    private List<AddressBook> addressBooks = new ArrayList<>();

    public List<AddressBook> getAddressBookList() {
        return addressBooks;
    }

    @Override
    public void startElement(String uri, String localName,
                             String qName, Attributes attributes) {

        if (LOGGER.isTraceEnabled())
            LOGGER.trace("Start Element : {}", qName);

        if (qName.toLowerCase().endsWith(ROOT)) {
            this.emails = new ArrayList<>();
            this.addressBooks = new ArrayList<>();
        }

        if (qName.toLowerCase().endsWith(ENTRY)) {
            this.isEntry = true;
        }

        if (qName.toLowerCase().endsWith(FULLNAME)) {
            isFullName = true;

        }

        if (qName.toLowerCase().endsWith(PHONENUMBER)) {
            this.isPhoneNumber = true;

        }

        if (qName.toLowerCase().endsWith(EMAIL)) {
            this.isEmail = true;
            int length = attributes.getLength();
            for (int n = 0; n < length; n++) {
                String attributeName = attributes.getQName(n);
                if (attributeName.toLowerCase().endsWith(ADDRESS)) {
                    String value = attributes.getValue(n);
                    emails.add(value);
                }
            }

        }
    }

    @Override
    public void endElement(String uri, String localName,
                           String qName) {

        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace("End Element : {}", qName);
        }

        if (qName.toLowerCase().endsWith(ENTRY) &&
                this.fullName != null) {
            final String localPhoneNumber = Objects.isNull(this.phoneNumber) || this.phoneNumber.trim().isEmpty() ? StringUtils.EMPTY : this.phoneNumber.trim();
            AddressBook addressBook = new AddressBook(this.fullName.replace(SPACE_CHAR, UNDERSCORE_CHAR),
                    this.fullName,
                    emails,
                    CollectionUtils.isNotEmpty(emails) ?
                            COMMENT_ABOUT_UNKNOWN_EMAIL :
                            localPhoneNumber);
            addressBooks.add(addressBook);
            this.fullName = null;
            this.isFullName = false;
            this.isEntry = false;
            this.isEmail = false;
            this.isPhoneNumber = false;
            this.phoneNumber = null;
            emails = new ArrayList<>();
        }
    }

    @Override
    public void characters(char[] ch, int start, int length) {

        if (this.isFullName) {
            this.fullName = new String(ch, start, length);
            this.isFullName = false;


        }

        if (this.isEntry) {
            this.isEntry = false;
            emails = new ArrayList<>();
        }

        if (this.isEmail) {
            this.isEmail = false;
        }

        if (this.isPhoneNumber) {
            this.phoneNumber = new String(ch, start, length);
            this.isPhoneNumber = false;
        }


    }

}