package at.mavila.gcontactsalp;

/**
 * Created by mavila on 1/7/17.
 */
final class AddressBookHandlerConstants {

    public static final String ROOT = "feed";
    public static final String FULLNAME = "fullname";
    public static final String ENTRY = "entry";
    public static final String PHONENUMBER = "phonenumber";
    public static final String EMAIL = "email";
    public static final String ADDRESS = "address";
    public static final String COMMENT_ABOUT_UNKNOWN_EMAIL = "Google Contacts imported";


    private AddressBookHandlerConstants() {
        //Forbidden to create instances of this class.
    }
}
