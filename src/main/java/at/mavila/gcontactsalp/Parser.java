package at.mavila.gcontactsalp;

import at.mavila.gcontactsalp.pojos.AddressBook;
import org.apache.commons.lang3.Validate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.Collections;
import java.util.List;
import java.util.function.Function;

import static java.nio.charset.StandardCharsets.UTF_8;
import static java.util.Objects.nonNull;

/**
 * Parser. This class receives the input stream to be parsed using the defined handler
 */
@Component("parser")
public final class Parser implements Function<InputStream, List<AddressBook>> {


    private static final Logger LOGGER = LoggerFactory.getLogger(Parser.class);
    private static final SAXParserFactory FACTORY = SAXParserFactory.newInstance();

    private static SAXParser saxParser;
    private static final AddressBookHandler ADDRESS_BOOK_HANDLER = new AddressBookHandler();


    /*Constants*/
    private static final String SAX_PARSER_MUST_NOT_BE_NULL = "SaxParser must not be null.";
    private static final String INPUT_STREAM_MUST_NOT_BE_NULL = "Input stream must not be null.";

    /*
     * Static block
     */
    static {
        try {
            saxParser = FACTORY.newSAXParser();
        } catch (Exception e) {
            saxParser = null;
            LOGGER.error(String.format("Exception caught: <%s>", e.getMessage()), e);
        }
    }


    private List<AddressBook> parse(final InputStream inputStream) throws IOException, SAXException {


        try (Reader reader = new InputStreamReader(Validate.notNull(inputStream, INPUT_STREAM_MUST_NOT_BE_NULL), UTF_8)) {

            InputSource is = new InputSource(reader);
            final String encoding = UTF_8.toString();
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("Setting the following encoding: {}", encoding);
            }
            is.setEncoding(encoding);
            LOGGER.debug("System ID: {}", is.getSystemId());
            Validate.notNull(saxParser, SAX_PARSER_MUST_NOT_BE_NULL).parse(is, ADDRESS_BOOK_HANDLER);

            return ADDRESS_BOOK_HANDLER.getAddressBookList();
        } finally {
            try {
                if (nonNull(inputStream))
                    inputStream.close();
            } catch (IOException e) {
                LOGGER.warn(String.format("IOException caught while closing the stream: %s", e.getMessage()), e);
            }
        }
    }

    @Override
    public List<AddressBook> apply(final InputStream inputStream) {
        try {
            return this.parse(inputStream);
        } catch (IOException | SAXException e) {
            LOGGER.error(String.format("Exception caught: <%s>", e.getMessage()), e);
            return Collections.emptyList();
        }
    }
}