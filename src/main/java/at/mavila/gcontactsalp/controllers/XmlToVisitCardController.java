package at.mavila.gcontactsalp.controllers;

import at.mavila.gcontactsalp.pojos.AddressBook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.function.Function;

@RestController
public class XmlToVisitCardController {

    private final Function<CharSequence, List<AddressBook>> xmlToListAddressBookConverter;
    private final Function<List<AddressBook>, CharSequence> visitCardConverter;

    @Autowired
    public XmlToVisitCardController(
            final Function<CharSequence, List<AddressBook>> xmlToListAddressBookConverter,
            final Function<List<AddressBook>, CharSequence> visitCardConverter) {
        this.xmlToListAddressBookConverter = xmlToListAddressBookConverter;
        this.visitCardConverter = visitCardConverter;
    }


    /**
     * Returns a vCard representation of the address book.
     * <pre>
     * curl google-parser-alpine.cfapps.io/importVisitCard -d @mutt.xml --header "Content-Type:text/xml"
     * curl localhost:8080/importVisitCard -d @mutt.xml --header "Content-Type:text/xml"
     * </pre>
     *
     * @param xml input Google address book representation
     * @return a CharSequece representing all cards in a single CharSequence, each card is separated based on the specification.
     */
    @RequestMapping(value = "/importVisitCard")
    public CharSequence doImportVisitCard(@RequestBody final String xml) {
        return this.visitCardConverter.apply(this.xmlToListAddressBookConverter.apply(xml));
    }
}
