package at.mavila.gcontactsalp.controllers;

import at.mavila.gcontactsalp.pojos.AddressBook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.function.Function;

@RestController
public class XmlToJsonController {

    private final Function<CharSequence, List<AddressBook>> xmlToListAddressBookConverter;


    @Autowired
    public XmlToJsonController(
            final Function<CharSequence, List<AddressBook>> xmlToListAddressBookConverter
    ) {
        this.xmlToListAddressBookConverter = xmlToListAddressBookConverter;
    }


    /**
     * To access this application use this curl command.
     * <pre>
     * curl google-parser-alpine.cfapps.io/importJson -d @mutt.xml --header "Content-Type:text/xml"
     * curl localhost:8080/importJson -d @mutt.xml --header "Content-Type:text/xml"
     * </pre>
     *
     * @param xml input Google address book representation.
     * @return a List of AddressBook objects.
     */
    @RequestMapping("/importJson")
    public List<AddressBook> doImport(@RequestBody final String xml) {
        return this.xmlToListAddressBookConverter.apply(xml);
    }
}
