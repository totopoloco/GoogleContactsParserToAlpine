package at.mavila.gcontactsalp.controllers;

import at.mavila.gcontactsalp.pojos.AddressBook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.function.Function;

@RestController
public class XmlToAlpineController {


    private final Function<CharSequence, List<AddressBook>> xmlToListAddressBookConverter;
    private final Function<List<AddressBook>, CharSequence> alpineConverter;


    @Autowired
    public XmlToAlpineController(
            final Function<CharSequence, List<AddressBook>> xmlToListAddressBookConverter,
            final Function<List<AddressBook>, CharSequence> alpineConverter) {
        this.xmlToListAddressBookConverter = xmlToListAddressBookConverter;
        this.alpineConverter = alpineConverter;
    }


    /**
     * Returns an Alpine representation of the address book.
     *
     * <pre>
     * curl google-parser-alpine.cfapps.io/importAlpine -d @mutt.xml --header "Content-Type:text/xml"
     * curl localhost:8080/importAlpine -d @mutt.xml --header "Content-Type:text/xml"
     * </pre>
     *
     * @param xml input Google address book representation.
     * @return a String with the Alpine addressbook.
     */
    @RequestMapping("/importAlpine")
    public CharSequence doImportAlpine(@RequestBody final String xml) {
        return this.alpineConverter.apply(this.xmlToListAddressBookConverter.apply(xml));

    }
}
