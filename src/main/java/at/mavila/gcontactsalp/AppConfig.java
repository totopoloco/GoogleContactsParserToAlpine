package at.mavila.gcontactsalp;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Created by 200000313 on 23.12.2016.
 */
@Component
public class AppConfig {

    @Value("${app.file.in}")
    private String appFileIn;

    @Value("${app.file.out}")
    private String appFileOut;



    public String getAppFileIn() {
        return appFileIn;
    }

    public String getAppFileOut() {
        return appFileOut;
    }


    //--Setters
    public void setAppFileOut(String appFileOut) {
        this.appFileOut = appFileOut;
    }

    public void setAppFileIn(String appFileIn) {
        this.appFileIn = appFileIn;
    }


    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
