package at.mavila.gcontactsalp.pojos;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.List;

/**
 * Created by mavila on 22.12.16.
 */
public class AddressBook implements Comparable<AddressBook> {

    private final String nickname;
    private final String fullName;
    private final List<String> address;
    private final String comment;

    public AddressBook(final String nickname, final String completeName, final List<String> address, final String comment) {

        this.nickname = nickname;
        this.fullName = completeName;
        this.address = address;
        this.comment = comment;

    }

    public String getComment() {
        return this.comment;
    }

    public String getNickname() {
        return this.nickname;
    }

    public String getFullName() {
        return this.fullName;
    }

    public List<String> getAddress() {
        return this.address;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (!(o instanceof AddressBook)) {
            return false;
        }

        return EqualsBuilder.reflectionEquals(this, o);

    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @Override
    public int compareTo(AddressBook o) {
        return this.getNickname().compareTo(o.getNickname());
    }
}