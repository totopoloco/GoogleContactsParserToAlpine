FROM openjdk:11
# The application's jar file
ARG JAR_FILE=build/libs/GoogleContactsParserToAlpine.jar
# Add the application's jar to the container
COPY ${JAR_FILE} GoogleContactsParserToAlpine.jar
# Expose port 8080
EXPOSE 8080/TCP
# Run the jar file
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-XX:+UnlockExperimentalVMOptions","-XX:+UseZGC","-jar","GoogleContactsParserToAlpine.jar"]
