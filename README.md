# GoogleContactsParserToAlpine
* Google contacts parser to Alpine .addressbook

* Parses a XML used by Goobook dump_contacts command and creates a .addressbook for Alpine email program

* Migrated from my other project to convert e.g. goobok dump_contacts and generate a file that can be used in Alpine as the address book.

* This new project uses Gradle as the build instead of Maven
* This new project uses Spring Boot, simplyfing all. Making the code cleaner will allow easily export to other formats in the future.

# TODO
* Google API instead of command line, command line looks like this: goobook dump_contacts > $DUMPED_FILE

# Goobook command
* The goobook command can be installed in Ubuntu like this:

```
sudo apt-get install goobook
```

# Scripting

When the server runs with SpringBoot, use the following commands for conversion

## Generating mutt.xml
```
goobook dump_contacts > mutt.xml
```
## Conversion
```
curl localhost:8080/importJson -d @mutt.xml --header "Content-Type:text/xml"
```
or
```
curl localhost:8080/importAlpine -d @mutt.xml --header "Content-Type:text/xml"
```
or
```
curl localhost:8080/importVisitCard -d @mutt.xml --header "Content-Type:text/xml"
```

# Notes
See the create_alpine_addressbook.sh to see how it is used in a UNIX environment.

[![Codacy Badge](https://api.codacy.com/project/badge/Grade/3b02b04e54a645398c38cdd1e7f03686)](https://www.codacy.com/app/marcoavilac/GoogleContactsParserToAlpine?utm_source=github.com&amp;utm_medium=referral&amp;utm_content=totopoloco/GoogleContactsParserToAlpine&amp;utm_campaign=Badge_Grade)
